//
//  FibonacciSequenceTableViewCell.m
//  FibonacciSequence
//
//  Created by Asa on 8/3/15.
//  Copyright (c) 2015 Psyaryu Inc. All rights reserved.
//

#import "FibonacciSequenceTableViewCell.h"
#import "FibonacciFactory.h"

@interface FibonacciSequenceTableViewCell()

@property (nonatomic, strong) FibonacciFactory *fibonacciFactory;
@property (nonatomic, strong) NSNumber *index;

@end

@implementation FibonacciSequenceTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWithFibonacciFactory:(FibonacciFactory *)fibonacciFactory withIndex:(NSNumber *)index {
    
    self.fibonacciSequenceLabel.text = @"Calculating....";
    self.fibonacciFactory = fibonacciFactory;
    self.index = index;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSNumber *fibonnacciSequenceNumber = [fibonacciFactory generateSequenceNumberForIndex:index];
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            self.fibonacciSequenceLabel.text = fibonnacciSequenceNumber.stringValue;
            
        });
        
    });
    
}

@end
