//
//  ViewController.m
//  FibonacciSequence
//
//  Created by Asa on 8/3/15.
//  Copyright (c) 2015 Psyaryu Inc. All rights reserved.
//

#import "ViewController.h"
#import "FibonacciFactory.h"
#import "FibonacciSequenceTableViewCell.h"

@interface ViewController ()

@property (nonatomic, strong) FibonacciFactory *fibonacciFactory;
@property (nonatomic, strong) NSNumber *fibonacciSequenceIndex;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.fibonacciFactory = [FibonacciFactory new];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate Protocol
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    self.fibonacciSequenceIndex = [NSNumber numberWithInteger:textField.text.integerValue];
    
    [self.fibonacciSequenceTableView reloadData];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL shouldChangeCharacters = YES;
    
    NSString *characters = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSCharacterSet *allCharactersExceptNumbersSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    NSRange rangeOfCharactersFromSet = [characters rangeOfCharacterFromSet:allCharactersExceptNumbersSet];
    BOOL containsIllegalCharacters = (rangeOfCharactersFromSet.location != NSNotFound);
    
    shouldChangeCharacters = !containsIllegalCharacters;
    
    return shouldChangeCharacters;
}

#pragma mark - UITableViewDataSource Protocol
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSInteger numberOfRows = self.fibonacciSequenceIndex.integerValue;

    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FibonacciSequenceTableViewCell *cellForRow = [tableView dequeueReusableCellWithIdentifier: NSStringFromClass([FibonacciSequenceTableViewCell class])];
    
    [cellForRow configureCellWithFibonacciFactory:self.fibonacciFactory withIndex:@(indexPath.row)];
    
    return cellForRow;
}

@end
