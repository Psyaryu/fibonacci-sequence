//
//  ViewController.h
//  FibonacciSequence
//
//  Created by Asa on 8/3/15.
//  Copyright (c) 2015 Psyaryu Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITextField *fibonacciSequenceIndexTextField;
@property (nonatomic, strong) IBOutlet UITableView *fibonacciSequenceTableView;

@end

