//
//  FibonacciFactory.m
//  FibonacciSequence
//
//  Created by Asa on 8/3/15.
//  Copyright (c) 2015 Psyaryu Inc. All rights reserved.
//

#import "FibonacciFactory.h"

@implementation FibonacciFactory

- (NSNumber *)generateSequenceNumberForIndex:(NSNumber *)fibonacciSequenceIndex {
    
    NSNumber *sequenceNumber;
    
    if (fibonacciSequenceIndex.integerValue == 0) {
        sequenceNumber = @0;
    } else if (fibonacciSequenceIndex.integerValue == 1) {
        sequenceNumber = @1;
    } else {
        
        unsigned int previousSequenceNumber = 0;
        unsigned int currentSequenceNumber = 1;

        // NOTE: index = 1 is skipping 2 iterations for iteration 0 and iteration 1
        for (NSInteger index = 1; index < fibonacciSequenceIndex.integerValue; index++) {
        
            unsigned int nextSequenceValue = previousSequenceNumber + currentSequenceNumber;
        
            previousSequenceNumber = currentSequenceNumber;
            currentSequenceNumber = nextSequenceValue;
        }
        
        sequenceNumber = @(currentSequenceNumber);

    }

    return sequenceNumber;
}

@end
