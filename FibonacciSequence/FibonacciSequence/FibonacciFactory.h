//
//  FibonacciFactory.h
//  FibonacciSequence
//
//  Created by Asa on 8/3/15.
//  Copyright (c) 2015 Psyaryu Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FibonacciFactory : NSObject

- (NSNumber *)generateSequenceNumberForIndex:(NSNumber *)fibonacciSequenceIndex;

@end

