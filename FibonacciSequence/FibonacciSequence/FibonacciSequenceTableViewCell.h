//
//  FibonacciSequenceTableViewCell.h
//  FibonacciSequence
//
//  Created by Asa on 8/3/15.
//  Copyright (c) 2015 Psyaryu Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FibonacciFactory;

@interface FibonacciSequenceTableViewCell : UITableViewCell

- (void)configureCellWithFibonacciFactory:(FibonacciFactory *)fibonacciFactory withIndex:(NSNumber *)index;

@property (nonatomic, strong) IBOutlet UILabel *fibonacciSequenceLabel;

@end
