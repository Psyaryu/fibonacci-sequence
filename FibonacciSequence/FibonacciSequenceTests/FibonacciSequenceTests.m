//
//  FibonacciSequenceTests.m
//  FibonacciSequenceTests
//
//  Created by Asa on 8/3/15.
//  Copyright (c) 2015 Psyaryu Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "FibonacciFactory.h"

@interface FibonacciSequenceTests : XCTestCase

@property (nonatomic, strong) FibonacciFactory *fibonacciFactory;

@end

@implementation FibonacciSequenceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.fibonacciFactory = [FibonacciFactory new];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    self.fibonacciFactory = nil;
}

- (void)testFibonacciFactoryGeneratesReturnsProperSequenceNumberForIndex0 {
    
    NSNumber *sequenceNumber = [self.fibonacciFactory generateSequenceNumberForIndex:@0];
    
    XCTAssert(sequenceNumber.integerValue == 0, @"Correct Sequence Value");
    
}

- (void)testFibonacciFactoryGeneratesReturnsProperSequenceNumberForIndex1 {
    
    NSNumber *sequenceNumber = [self.fibonacciFactory generateSequenceNumberForIndex:@1];
    
    XCTAssert(sequenceNumber.integerValue == 1, @"Correct Sequence Value");
    
}

- (void)testFibonacciFactoryGeneratesReturnsProperSequenceNumberForIndex2 {
    
    NSNumber *sequenceNumber = [self.fibonacciFactory generateSequenceNumberForIndex:@2];
    
    XCTAssert(sequenceNumber.integerValue == 1, @"Correct Sequence Value");
    
}

- (void)testFibonacciFactoryGeneratesReturnsProperSequenceNumberForIndex3 {
    
    NSNumber *sequenceNumber = [self.fibonacciFactory generateSequenceNumberForIndex:@3];
    
    XCTAssert(sequenceNumber.integerValue == 2, @"Correct Sequence Value");
    
}

@end
